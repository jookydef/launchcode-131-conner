package lab1;
import cse131.ArgsProcessor;

public class nutrition {

	public static void main(String[] args) {
		ArgsProcessor ap = new ArgsProcessor(args);
		String name = ap.nextString("Enter your food name");
		
		
		double fatGrams = ap.nextDouble("Enter the grams of fat");
		double carbgrams = ap.nextDouble("Enter the grams of carbs");
		double proGrams = ap.nextDouble("Enter the grams of protein");
		double statedCals = ap.nextDouble("Enter the total calories on the label");
		
		double fatCals = fatGrams *9;
		double carbCals= carbgrams *4;
		
		double proCals = proGrams *4;		
		double totalCals = fatCals + carbCals + proCals;
		
		double fiberGrams = (totalCals - statedCals)/4;
		
		double fatPercent = (fatCals/statedCals) * 100;
		double carbPercent = (carbCals/statedCals) * 100;
		double proPercent = (proCals/statedCals) * 100;
		
		double fiberCals= fiberGrams *4;
		
		
		System.out.println("This food " + name + "is said to have " + (int)statedCals+ " (available) Calories.");
		System.out.println("With " +(double)Math.round(fiberCals*100)/100+" unavailable Calories, this food has "+ (double)Math.round(fiberGrams*100)/100 +" grams of fiber.");
		System.out.println("");
		System.out.println("Approximately");
		System.out.println((double)Math.round(fiberCals*100)/100  + " calories of fiber");
		System.out.println((double)Math.round(fatCals*100)/100  + " calories of fat");
		System.out.println((double)Math.round(proCals*100)/100  + " calories of protein");
		System.out.println((double)Math.round(carbCals*100)/100  + " calories of carbohydrates");
		System.out.println((double)Math.round(totalCals*100)/100  + " total calories");
		System.out.println("");
		System.out.println((double)Math.round(carbPercent*10)/10 + "% of your food is carbs");
		System.out.println((double)Math.round(fatPercent*10)/10 + "% of your food is fat");
		System.out.println((double)Math.round(proPercent*10)/10  + "% of your food is protein");
		System.out.println("");
		System.out.println(fatGrams + " grams of fat");
		System.out.println(carbgrams + " grams of carbs");
		System.out.println(proGrams + " grams of protein");
		System.out.println((double)Math.round(fiberGrams*100)/100 + " grams of fiber");
		
boolean lowCarb = ((double)Math.round(carbPercent*10)/10) < 25 ;
boolean lowFat = ((double)Math.round(fatPercent*10)/10) < 15 ;
boolean coinFlip = (int) Math.floor(Math.random() * 2) < 1;


		System.out.println("");
		System.out.println("This food is acceptable for a low-carb diet? "+ lowCarb);
		System.out.println("This food is acceptable for a low-fat diet?  "+ lowFat);
		
		System.out.println("By coin flip, you should eat this food?" + coinFlip);
	}

}
